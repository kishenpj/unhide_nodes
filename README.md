# Unlock Nodes

## Description
A simple tool that unlocks hidden/deprecated nodes in houdini which still might be useful or you just miss them.
Some nodes might not be compatible with your current version of houdini and **WILL** make your scene unstable. Use with caution!

  ![Preview](https://gitlab.com/kishenpj/unlock-nodes/-/raw/master/UnNo_demo.gif)

## Installation
 * [Download](https://gitlab.com/kishenpj/unhide_nodes/-/archive/master/unhide_nodes-master.zip) the zip file and extract to path **`"$HOME/<houdini_version>/scripts/python"`** (create missing folders if necessary)
 * Rename the folder to `'unhide_nodes'` 
 * After extracting, move the `MainMenuCommon.xml` file to houdini home folder or [append](https://www.sidefx.com/docs/houdini/basics/config_menus.html)
 it to your existing xml file
 * Restart Houdini and you should it in 'FX' menu