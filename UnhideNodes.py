import hou
from PySide2 import QtCore
from PySide2.QtCore import Qt, QSortFilterProxyModel, QRegExp
from PySide2.QtGui import QCursor, QStandardItemModel, QStandardItem
from PySide2.QtWidgets import QWidget, QComboBox, \
    QAbstractItemView, QPushButton, QTreeView, QGridLayout, QLineEdit, QApplication


class NodesTreeView(QTreeView):

    def __init__(self, parent=None):
        QTreeView.__init__(self, parent)
        self.setStyleSheet(hou.qt.styleSheet())
        self.setStyleSheet("QTreeView::item:hover {background: rgb(85,70,35);}"
                           "QTreeView {font: 11pt;}"
                           "QTreeView::item {padding: 1px 1px 1px 1px}");
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setRootIsDecorated(False)
        self.setHeaderHidden(True)
        self.model = QStandardItemModel()
        self.proxyModel = QSortFilterProxyModel()
        self.proxyModel.setDynamicSortFilter(True)
        self.proxyModel.setSourceModel(self.model)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setModel(self.proxyModel)


class UnhideNodes(QWidget):

    def __init__(self):
        super(UnhideNodes, self).__init__()
        self.build()
        self.node_search.setFocus()
        self.connections()
        self.get_node_list()

    def build(self):
        self.setWindowTitle("Unhide Nodes")
        self.setWindowIcon(hou.qt.Icon("TOP_perforce"))
        self.setGeometry(QCursor.pos().x(), QCursor.pos().y(), 300, 500)
        self.setContentsMargins(5, 5, 5, 5)
        self.setParent(hou.ui.mainQtWindow(), QtCore.Qt.Window)
        self.setStyleSheet(hou.qt.styleSheet())

        self.main_layout = QGridLayout()
        self.main_layout.setVerticalSpacing(10)
        self.main_layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.main_layout)

        self.category_combo = QComboBox()
        self.category_combo.addItems(["Sop", "Cop2", "Dop", "Object", "Shop", "Chop", "Vop"])
        self.main_layout.addWidget(self.category_combo, 0, 0)

        self.node_list = NodesTreeView()
        self.main_layout.addWidget(self.node_list, 1, 0)

        self.node_search = QLineEdit()
        self.node_search.setContextMenuPolicy(Qt.CustomContextMenu)
        self.node_search.setClearButtonEnabled(True)
        self.node_search.setPlaceholderText("Search..")
        self.main_layout.addWidget(self.node_search, 1, 0, 1, 1, Qt.AlignBottom)

        self.unlock_btn = QPushButton("Unhide")
        self.main_layout.addWidget(self.unlock_btn, 2, 0, Qt.AlignCenter)

    def connections(self):
        self.category_combo.currentIndexChanged.connect(self.get_node_list)
        self.node_search.textChanged.connect(self.find_node)
        self.unlock_btn.clicked.connect(self.unlock_nodes)

    def find_node(self):
        text = self.node_search.text()
        self.node_list.proxyModel.setFilterRegExp(QRegExp(text, Qt.CaseInsensitive, QRegExp.FixedString));

    def get_node_list(self):
        category = self.category_combo.currentText()
        node_list = hou.hscript("opunhide %s" % category)[0].split()[2::3]
        if len(node_list) > 0:
            self.node_list.model.clear()
            self.node_list.setEnabled(True)
            for node in node_list:
                item = QStandardItem(node)
                self.node_list.model.appendRow(item)

    def get_selection(self):
        items = self.node_list.selectedIndexes()
        item_list = []
        for item in items:
            item_list.append(item.data())
            item_list.sort()
        return item_list

    def unlock_nodes(self):
        item_list = self.get_selection()
        for item in item_list:
            hou.hscript("opunhide {0} {1}".format(self.category_combo.currentText(), item))
            hou.ui.setStatusMessage("Unlocked nodes : %s" % (' / '.join([str(elem) for elem in item_list])))
        self.get_node_list()


for my_app in QApplication.allWidgets():
    if type(my_app).__name__ == 'UnhideNodes':
        my_app.close()
mainWin = UnhideNodes()
mainWin.show()
